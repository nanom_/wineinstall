Wineinstall
===
Wineinstall is a graphical Wine installer for Linux. It's intended to be used
by newbie users who find difficult to install Wine through the terminal. It
currently works on the following operating systems:
* Ubuntu and any Ubuntu-based Linux distro (Mint, Zorin, PopOS, etc.)

![gif](wineinstall.gif)

Download now and install Wine!
---
Clic the button below to download Wineinstall:

[![Download for Ubuntu-based distros](
https://img.shields.io/badge/Download%20now-Ubuntu%2C%20Mint%2C%20Ubuntu--based%20distros-red?style=for-the-badge&logo=ubuntu)](
https://gitlab.com/nanom_/wineinstall/-/jobs/2064783308/artifacts/raw/build/wineinstall-deb.deb)

It is also available on the
[releases page](https://gitlab.com/nanom_/wineinstall/-/releases).
Most "just works" distros have a graphical installer for .deb files called
gdebi. Just install the package and run `wineinstall` from your terminal
to begin the installation process.

If gdebi is not installed on your system, run the following command
to install the installer: `sudo dpkg -i wineinstall.deb`

Dependencies
---
* zenity for the GUI (will be automatically installed)
* pkexec (usually already installed)
* wget (usually already installed)

Ok but now I want to uninstall Wine
---
```
sudo apt purge winehq-stable
```

Shellcheck
---
I don't really care about shellchecking the scripts right now because most
warnings I get are irrelevant and require a shellcheckrc to be fixed.
I might add a shellcheckrc later to fix this, but the installer just works.

Are you going to add support for other distros?
---
Perhaps.
Distros that new Linux users tend to use will have a higher priority.
